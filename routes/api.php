<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Auth\LoginController;
use App\Http\Controllers\API\Auth\RegisterController;
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\API\TaskController;
use App\Http\Controllers\API\WorkspaceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', [RegisterController::class, 'register'])->name('register');
    Route::post('login', [LoginController::class, 'login']);
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('logout', [LoginController::class, 'logout']);
    });
    Route::post('refresh', [LoginController::class, 'refresh']);
});

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('profile', [ProfileController::class, 'index']);

    Route::group(['prefix' => 'workspace'], function () {
        Route::post('/', [WorkspaceController::class, 'store'])->name('create_workspace');
        Route::get('/', [WorkspaceController::class, 'getAll'])->name('get_all_workspaces');
        Route::get('/{id}', [WorkspaceController::class, 'index'])->name('get_workspace');
        Route::put('/{id}', [WorkspaceController::class, 'update'])->name('update_workspace');
        Route::delete('/{id}', [WorkspaceController::class, 'delete'])->name('delete_workspace');
        Route::post('invite/{id}', [WorkspaceController::class, 'invite'])->name('invite_friends_workspace');
        Route::delete('remove/{id}', [WorkspaceController::class, 'remove'])->name('remove_friends_workspace');

        Route::group(['prefix' => 'task'], function () {
            Route::get('/{id}', [TaskController::class, 'index']);
            Route::post('/', [TaskController::class, 'store']);
            Route::put('/{id}', [TaskController::class, 'update']);
            Route::delete('/{id}', [TaskController::class, 'delete']);
            Route::post('/assign', [TaskController::class, 'assignTask']);
            Route::delete('/delete/{id}', [TaskController::class, 'deleteAssign']);
        });
    });
});


// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
