<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserWorkspace extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'user_workspaces';
    protected $fillable = [
        'id',
        'workspace_id',
        'user_id',
        'role'
    ];


    public function workspace()
    {
        $this->belongsToMany(Workspace::class);
    }

    public function user()
    {
        $this->belongsToMany(User::class);
    }
}
