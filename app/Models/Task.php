<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = "tasks";
    protected $fillable = [
        'id',
        'user_id',
        'workspace_id',
        'title',
        'description',
        'status',
        'progress',
        'label',
        'milestone'
    ];

    public function workspace()
    {
        return $this->belongsTo(Workspace::class, 'workspace_id', 'id');
    }

    public function userTask()
    {
        return $this->hasMany(UserTask::class, 'task_id', 'id');
    }
}
