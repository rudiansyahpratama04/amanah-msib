<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{
    protected $table = "workspaces";
    protected $casts = [
        'id' => 'string',
    ];
    protected $fillable = [
        'id',
        'name',
        'description',
        'owner_id',
        'visibility',
    ];
    public $incrementing = false;
    public function getCasts()
    {
        if ($this->incrementing) {
            return array_merge([
                $this->getKeyName() => 'int',
            ], $this->casts);
        }
        return $this->casts;
    }

    public function userWorkspace()
    {
        return $this->hasMany(UserWorkspace::class, 'workspace_id', 'id');
    }

    public function task()
    {
        return $this->hasMany(Task::class, 'workspace_id', 'id');
    }
}
