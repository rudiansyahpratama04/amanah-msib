<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\RepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserRepository
{

  public function register($request)
  {
    $uuid = Str::uuid()->toString();
    $dataUser           = [
      'first_name'        => $request->first_name,
      'last_name'         => $request->last_name,
      'username'          => $request->username,
      'email'             => $request->email,
      'phone_number'      => $request->phone_number,
      'password'          => Hash::make($request->password),
    ];
    $user = User::create($dataUser);
    return $user;
  }
}
