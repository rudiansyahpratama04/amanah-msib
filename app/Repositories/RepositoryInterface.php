<?php

namespace App\Repositories;

use Illuminate\Http\Request;

interface RepositoryInterface
{

  public function show($id);
  public function showAll();
  public function delete($id);

  public function update($id, array $data);
  public function store(array $data);
}
