<?php

namespace App\Http\Requests\API\Task;

use App\Helpers\ResponseFormatter;
use App\Models\Task;
use App\Utilities\StatusUtilities;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class CreateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => ['required'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = response()->json([
            'code'      => StatusUtilities::BAD_REQUEST,
            'info'      => $validator->messages()->first(),
            'data'      => ['errors' => $validator->messages()->toArray()]
        ]);
        throw new ValidationException($validator, $response);
    }
}
