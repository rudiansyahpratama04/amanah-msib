<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utilities\StatusUtilities;
use App\Http\Requests\API\Auth\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['login', 'register', 'refresh']]);
    }

    public function login(LoginRequest $request)
    {
        $credentialsEmail = request(['email', 'password']);
        // , ['exp' => Carbon\Carbon::now()->addDays(7)->timestamp]
        $tokenEmail     = auth('api')->attempt($credentialsEmail);
        $token          = $tokenEmail;

        if (!$tokenEmail) {
            return response()->json([
                'code'  => StatusUtilities::FAILED,
                'info'  => "Email atau password yang anda masukan salah",
            ], 400);
        }

        $user       = auth('api')->user();

        if ($user->is_block == 1) {
            auth('api')->logout();
            return response()->json([
                'code'  => StatusUtilities::USER_BLOCK,
                'info'  => "Akun anda diblokir",
            ], 403);
        }

        // User Child tidak bisa login
        if (!empty($user->parent_id)) {
            auth('api')->logout();
            return response()->json([
                'code'  => StatusUtilities::USER_BLOCK,
                'info'  => "Maaf anda tidak bisa login lewat versi mobile. Silahkan login lewat web.",
            ], 400);
        }


        return $this->respondWithToken($token, "Login berhasil");
    }

    public function refresh()
    {
        try {
            $token = auth('api')->refresh();
        } catch (JWTException $e) {
            return response()->json([
                'code'  => StatusUtilities::FAILED,
                'info'  => "Token could not be parsed from the request."/*$e->getMessage(),*/
            ], 400);
        }
        return $this->respondWithToken($token, "Refresh token berhasil");
    }


    public function logout()
    {
        auth('api')->logout();
        return response()->json([
            'code'      => 200,
            'info'      => 'Successfully logged out',
            'data'      => new \stdClass(),
        ], 200);
    }

    protected function respondWithToken($token, $message = "")
    {
        $user = auth('api')->user();
        // $user->load('alamat', 'alamat.district');
        return response()->json([
            'code'          => StatusUtilities::SUCCESS,
            'info'          => $message,
            'data'          => [
                'access_token'  => $token,
                'token_type'    => 'bearer',
                'user'          =>  [
                    'username' => $user->username,
                    'email' => $user->email,
                ],
                'expires_in'    => auth('api')->factory()->getTTL() * 60,
            ],
        ]);
    }
}
