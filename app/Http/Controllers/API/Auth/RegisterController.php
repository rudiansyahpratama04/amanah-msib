<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\UserRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $code = "00";
        $data = [];
        try{
            $code = "10";
            $rules = [
                "first_name" => "required",
                "last_name" => "required",
                "username" => "required|unique:users,username",
                "email" => "required|unique:users,email",
                "phone_number" => "required|numeric|unique:users,phone_number",
                "password" => "required|confirmed",
            ];
        
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()){
                return response()->json([
                    "code" => "-1",
                    "info" => $validator->messages()->first(),
                    "data" => [
                        "errors" => $validator->messages()->toArray()
                        ]
                ], 400);
            }
            $userRepo = new UserRepository();
            $user = $userRepo->register($request);
            return response()->json([
                'code'      => 200,
                'info'      => 'Register berhasil',
                'data'      => $user,
            ]);
        }catch(Exception $e){
            return response()->json([
                "code" => "-1",
                "info" => "Register Gagal",
                "data" => null
            ], 500);
        }
    }
}
