<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\User;
use App\Models\UserTask;
use App\Models\UserWorkspace;
use App\Models\Workspace;
use App\Utilities\StatusUtilities;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class WorkspaceController extends Controller
{

    public function getAll()
    {
        $data = [];
        $user = auth('api')->user();
        $userWorkspaces = UserWorkspace::where('user_id', $user->id)->get();
        foreach ($userWorkspaces as $userWorkspace) {
            $workspace = Workspace::with('userWorkspace')->where('id', $userWorkspace->workspace_id)->first();
            array_push($data, $workspace);
        }
        $workspaces = $data;
        if (!empty($workspaces)) {
            return response()->json([
                'code' => 200,
                'info' => 'Get all workspaces berhasil',
                'data' => $workspaces
            ], 200);
        } else {
            return response()->json([
                'code' => 404,
                'info' => 'Workspace not found',
            ], 404);
        }
    }

    public function index(Request $request, $id)
    {
        try {
            $user = auth('api')->user();
            $data = [];
            $workspace = Workspace::with('userWorkspace')->where('id', $id)->first();
            foreach ($workspace->userWorkspace as $userWorkspace) {
                $user = User::where('id', $userWorkspace->user_id)->first();
                $userWorkspace['email'] = $user->email;
                $userWorkspace['username'] = $user->username;
            }
            $tasks = Task::where('workspace_id', $id);
            $acc_granted = 0;
            foreach ($workspace->userWorkspace as $val) {
                if ($val->user_id == $user->id) {
                    $acc_granted = 1;
                    break;
                }
            }
            if ($workspace->owner_id == $user->id || $acc_granted == 1) {
                $field = $request->query('field', 'created_at');
                $sort = $request->query('sort', 'ascend');
                if ($request->filled('user_id')) {
                    $userTasks = UserTask::select('task_id')->where('user_id', $request->user_id)->get();
                    $tasks = $tasks->whereIn('id', $userTasks);
                }
                if ($request->filled('task_id')) {
                    $tasks = $tasks->where('task_id', $request->task_id);
                }
                if ($request->filled('progress')) {
                    $tasks = $tasks->where('progress', $request->progress);
                }
                if ($request->filled('title')) {
                    $tasks = $tasks->where('title', 'LIKE', '%' . $request->search . '%');
                }
                if ($request->filled('label')) {
                    $tasks = $tasks->where('label', $request->label);
                }
                if ($request->filled('milestone')) {
                    $tasks = $tasks->where('milestone', $request->milestone);
                }

                if ($sort === "ascend") {
                    $tasks = $tasks->orderBy($field);
                } else {
                    $tasks = $tasks->orderBy($field, 'desc');
                }

                $duplicate = clone $tasks;
                if ($request->filled('milestone')) {
                    $all = $duplicate->count();
                    $count = $duplicate->where('progress', 'DONE')->count();
                    $workspace['milestone_progress_in_percent'] = ((float) $count / (float) $all) * 100;
                }
                $data['workspace'] = $workspace;
                $tasks = $tasks->get();
                $data['tasks'] = $tasks;
                return response()->json([
                    'code' => 200,
                    'info' => "Berhasil mendapatkan workspace",
                    "data" => $data
                ], 200);
            } else {
                return response()->json([
                    'code' => 403,
                    'info' => "User forbidden access",
                ], 403);
            }
        } catch (\Exception $e) {
            return response()->json([
                'code' => StatusUtilities::BAD_REQUEST,
                'info' => $e->getMessage(),
            ]);
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = auth('api')->user();
            $uuid = Uuid::generate()->string;
            $data = [
                "id" => $uuid,
                "name" => $request->name,
                "description" => $request->description,
                "owner_id" => $user->id,
            ];
            $workspace = Workspace::create($data);
            $dataWorkspace = [
                "workspace_id" => $workspace->id,
                "user_id" => $user->id,
                "role" => "owner"
            ];
            $userWorkspace = UserWorkspace::create($dataWorkspace);
            $response = Workspace::with('userWorkspace')->with('task')->where('id', $workspace->id)->get();
            DB::commit();
            return response()->json([
                'code' => 200,
                'info' => 'Workspace berhasil dibuat',
                'data' => $response
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'code' => StatusUtilities::FAILED,
                'info' => $e->getMessage(),
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $user = auth('api')->user();
            $workspace = Workspace::with('userWorkspace')->with('task')->where('id', $id)->where('owner_id', $user->id)->first();

            if (!empty($workspace)) {
                $workspace->name = $request->name;
                $workspace->description = $request->description;
                $workspace->visibility = $request->visibility;
                $workspace->save();
                DB::commit();
                return response()->json([
                    'code' => 200,
                    'info' => 'Workspace berhasil diubah',
                    'data' => $workspace
                ], 200);
            } else {
                DB::rollBack();
                return response()->json([
                    'code' => 404,
                    'info' => 'Workspace tidak ditemukan',
                ], 404);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'code' => StatusUtilities::FAILED,
                'info' => $e->getMessage(),
            ]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $user = auth('api')->user();
            $workspace = Workspace::where('id', $id)->where('owner_id', $user->id)->first();

            if (!empty($workspace)) {
                $userWorkspaces = UserWorkspace::where('workspace_id', $workspace->id)->get();
                foreach ($userWorkspaces as $userWorkspace) {
                    $userWorkspace->delete();
                }
                $workspace->delete();
                DB::commit();
                return response()->json([
                    'code' => 200,
                    'info' => 'Workspace berhasil dihapus'
                ], 200);
            } else {
                Db::rollBack();
                return response()->json([
                    'code' => 404,
                    'info' => 'Workspace tidak ditemukan',
                ], 404);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'code' => StatusUtilities::FAILED,
                'info' => $e->getMessage
            ]);
        }
    }

    public function invite(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $workspace = Workspace::where('id', $id)->where('visibility', 'team')->first();
            if (empty($workspace)) {
                return response()->json([
                    'code' => 404,
                    'info' => 'Workspace tidak ditemukan'
                ], 404);
            }
            $user = User::where('email', $request->email)->first();
            $owner = auth('api')->user();
            if (!empty($user)) {
                if ($workspace->owner_id == $owner->id) {
                    $checkUser = UserWorkspace::where('workspace_id', $workspace->id)->where('user_id', $user->id)->first();
                    if (!empty($checkUser)) {
                        DB::rollBack();
                        return response()->json([
                            'code' => 500,
                            'info' => 'User telah ada dalam workspace'
                        ]);
                    } else {
                        $data = [
                            'workspace_id' => $workspace->id,
                            'user_id' => $user->id,
                            'role' => 'user'
                        ];
                        $userWorkspace = UserWorkspace::create($data);
                        DB::commit();
                        return response()->json([
                            'code' => 200,
                            'info' => 'Berhasil menambahkan user dari workspace',
                            'data' => $userWorkspace
                        ], 200);
                    }
                } else {
                }
            } else {
                DB::rollBack();
                return response()->json([
                    'code' => 404,
                    'info' => 'User tidak ditemukan'
                ], 404);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'code' => StatusUtilities::FAILED,
                'info' => $e->getMessage()
            ]);
        }
    }

    public function remove(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $workspace = Workspace::where('id', $id)->first();
            if (empty($workspace)) {
                return response()->json([
                    'code' => 404,
                    'info' => 'Workspace tidak ditemukan'
                ], 404);
            }
            $user = User::where('email', $request->email)->first();
            if (!empty($user)) {
                $userWorkspace = UserWorkspace::where('workspace_id', $workspace->id)->where('user_id', $user->id)->first();
                $userWorkspace->delete();
                DB::commit();
                return response()->json([
                    'code' => 200,
                    'info' => 'Berhasil menghapus user dari workspace',
                ], 200);
            } else {
                return response()->json([
                    'code' => 404,
                    'info' => 'User tidak ditemukan'
                ], 404);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'code' => StatusUtilities::FAILED,
                'info' => $e->getMessage(),
            ]);
        }
    }
}
