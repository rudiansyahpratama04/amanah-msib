<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Utilities\StatusUtilities;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProfileController extends Controller
{
    public function index()
    {
        try {
            $user = auth('api')->user();
            return response()->json([
                'code'          => StatusUtilities::SUCCESS,
                'info'          => "Mengambil profile berhasil",
                'data'          => [
                    'user'          =>  [
                        'firstname' => $user->first_name,
                        'lastname' => $user->last_name,
                        'username' => $user->username,
                        'email' => $user->email,
                        'phone_number' => $user->phone_number,
                    ]
                ],
            ]);
        } catch (\Throwable $th) {
            Log::debug($th);
            return response()->json([
                'code'      => StatusUtilities::FAILED,
                'info'      => 'Mengambil profile gagal',
                'data'      => new \stdClass(),
            ], 400);
        }
    }
}
