<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Task\CreateTaskRequest;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\User;
use App\Models\UserTask;
use App\Models\UserWorkspace;
use App\Models\Workspace;
use App\Utilities\StatusUtilities;
use GuzzleHttp\Promise\Create;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use stdClass;

class TaskController extends Controller
{
    public function index($id)
    {
        $data = [];
        $user = auth('api')->user();
        $task = Task::with('userTask')->where('id', $id)->first();
        foreach ($task->userTask as $userTask) {
            $userAsignee = User::find($userTask->user_id);
            $userTask['email'] = $userAsignee->email;
            $userTask['username'] = $userAsignee->username;
        }
        if (!empty($task)) {
            $userWorkspace = UserWorkspace::where('workspace_id', $task->workspace_id)->where('user_id', $user->id)->first();
            if (!empty($userWorkspace)) {
                return response()->json([
                    'code'          => StatusUtilities::SUCCESS,
                    'info'          => "Mengambil task berhasil",
                    'data'          => $task
                ], 200);
            } else {
                return response()->json([
                    'code'          => StatusUtilities::SUCCESS,
                    'info'          => "Task tidak ditemukan",
                ], 404);
            }
        } else {
            return response()->json([
                'code' => 404,
                'info' => "Task tidak ditemukan"
            ], 404);
        }
    }

    public function store(CreateTaskRequest $request)
    {
        DB::beginTransaction();
        try {
            $user_id = auth('api')->id();

            $workspace = Workspace::where('id', $request->workspace_id)->first();

            if ($workspace->owner_id == $user_id) {
                $task = new Task();
                $task->user_id = $user_id;
                $task->workspace_id = $request->workspace_id;
                $task->title = $request->title;
                $task->description = $request->description;
                $task->status = $request->status;

                $task->save();
                DB::commit();
                return response()->json([
                    'code'          => StatusUtilities::SUCCESS,
                    'info'          => "Membuat task berhasil",
                    'data'          => $task
                ]);
            } else {
                DB::rollBack();
                return response()->json([
                    'code' => 403,
                    "info" => "Anda tidak diizinkan untuk membuat task di workspace ini",
                ], 403);
            }
        } catch (\Throwable $th) {
            Log::debug($th);
            DB::rollBack();
            return response()->json([
                'code'          => StatusUtilities::FAILED,
                'info'          => "Membuat task gagal",
                'data'          => new stdClass
            ]);
        }
    }

    public function update(CreateTaskRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $task = Task::with('userTask')->find($id);
            $task->title = $request->title;
            $task->description = $request->description;
            $task->status = $request->status;
            $task->label = $request->label;
            $task->milestone = $request->milestone;
            $task->progress = $request->progress;
            $task->save();
            DB::commit();
            return response()->json([
                'code'          => StatusUtilities::SUCCESS,
                'info'          => "Update task berhasil",
                'data'          => $task
            ]);
        } catch (\Throwable $th) {
            Log::debug($th);
            DB::rollBack();
            return response()->json([
                'code'          => StatusUtilities::FAILED,
                'info'          => "Update task gagal",
                'data'          => new stdClass
            ]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $user = auth('api')->user();
            $task = Task::where('id', $id)->first();
            $userWorkspace = UserWorkspace::where('workspace_id', $task->workspace_id)->where('user_id', $user->id)->first();
            if (!empty($userWorkspace)) {
                $task = Task::find($id);
                $task->delete();
                DB::commit();
                return response()->json([
                    'code'          => StatusUtilities::SUCCESS,
                    'info'          => "Menghapus task berhasil",
                ], 200);
            } else {
                return response()->json([
                    'code'          => 404,
                    'info'          => "Task tidak ditemukan",
                ], 404);
            }
        } catch (\Throwable $th) {
            Log::debug($th);
            DB::rollBack();
            return response()->json([
                'code'          => StatusUtilities::FAILED,
                'info'          => "Menghapus task gagal",
                'data'          => new stdClass
            ]);
        }
    }

    public function assignTask(Request $request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                "task_id" => "required|exists:tasks,id",
                "user_id" => "required|exists:users,id",
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    "code" => "-1",
                    "info" => $validator->messages()->first(),
                    "data" => [
                        "errors" => $validator->messages()->toArray()
                    ]
                ], 400);
            }

            $task = Task::with('userTask')->where('id', $request->task_id)->first();
            $userCheck = UserTask::where('user_id', $request->user_id)->where('task_id', $request->task_id)->first();
            if (!empty($userCheck)) {
                DB::rollBack();
                return response()->json([
                    'code' => 400,
                    'info' => "User sudah diassign ke dalam task",
                ], 400);
            } else {
                if (!empty($task)) {
                    $data = [
                        'user_id' => $request->user_id,
                        'task_id' => $task->id,
                    ];
                    $userTask = UserTask::create($data);
                    $taskUpdate = Task::with('userTask')->where('id', $request->task_id)->first();
                    DB::commit();
                    return response()->json([
                        'code'          => StatusUtilities::SUCCESS,
                        'info'          => "Assigning task berhasil",
                        'data'          => $taskUpdate
                    ]);
                } else {
                    DB::rollBack();
                    return response()->json([
                        'code' => 404,
                        'info' => "Task tidak ditemukan",
                    ]);
                }
            }
        } catch (\Throwable $th) {
            Log::debug($th);
            DB::rollBack();
            return response()->json([
                'code'          => StatusUtilities::FAILED,
                'info'          => "Assigning Task Gagal",
                'data'          => new stdClass
            ]);
        }
    }

    public function deleteAssign(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $rules = [
                "user_id" => "required|exists:users,id",
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    "code" => "-1",
                    "info" => $validator->messages()->first(),
                    "data" => [
                        "errors" => $validator->messages()->toArray()
                    ]
                ], 400);
            }

            $user = auth('api')->user();
            $task = Task::find($id);
            if (!empty($task)) {
                if ($task->user_id == $user->id) {
                    $userTask = UserTask::where('task_id', $id)->where('user_id', $request->user_id)->first();
                    if (!empty($userTask)) {
                        $userTask->delete();
                        DB::commit();
                        return response()->json([
                            'code'          => StatusUtilities::SUCCESS,
                            'info'          => "Delete asignee berhasil",
                        ], 200);
                    } else {
                        DB::rollBack();
                        return response()->json([
                            'code' => 404,
                            'info' => "User tidak ditemukan",
                        ]);
                    }
                } else {
                    DB::rollBack();
                    return response()->json([
                        'code' => 403,
                        'info' => "User tidak diizinkan untuk menghapus task"
                    ], 403);
                }
            } else {
                DB::rollBack();
                return response()->json([
                    'code' => 404,
                    'info' => "Task tidak ditemukan",
                ]);
            }
        } catch (\Throwable $th) {
            Log::debug($th);
            DB::rollBack();
            return response()->json([
                'code'          => StatusUtilities::FAILED,
                'info'          => "Delete Asignee Task Gagal",
                'data'          => new stdClass
            ], 500);
        }
    }
}
