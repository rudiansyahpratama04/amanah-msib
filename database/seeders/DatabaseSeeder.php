<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'testing',
            'last_name' => 'doang',
            'username' => 'testing',
            'email' => 'testing@gmail.com',
            'phone_number' => "081234567890",
            'password' => Hash::make('password'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'mamank',
            'last_name' => 'hehe',
            'username' => 'mamankhehe',
            'email' => 'mamankhehe@gmail.com',
            'phone_number' => "08987654321",
            'password' => Hash::make('hehe1234'),
        ]);
    }
}
